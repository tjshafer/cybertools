<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->text('thumbnail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): bool
    {
        return true;
    }
};

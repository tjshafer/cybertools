<?php

namespace App\Http\Livewire\Tools;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class HTTPStatusCodeChecker extends Component
{
    public string $status = 'none';

    public int    $code = 0;

    public string $domain = '';

    public function submit(): void
    {
        if ($this->domain) {
            try {
                $response = Http::get($this->domain);

                $this->code = $response->status();

                $this->sendRedirect($response);
            } catch(\Exception $e) {
                $this->status = 'Unavailable';
                $this->code = 0;
            }
        }
    }

    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('modules.tools.http-status-code-checker.livewire');
    }

    public function sendRedirect($response): void
    {
        if ($response->successful()) {
            $this->status = 'OK';
        } elseif ($response->redirect()) {
            $this->status = 'Redirect';
        } elseif ($response->clientError()) {
            $this->status = 'Client Error';
        } elseif ($response->serverError()) {
            $this->status = 'Server Error';
        } else {
            $this->status = 'Unavailable';
        }
    }
}

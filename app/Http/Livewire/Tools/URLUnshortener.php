<?php

namespace App\Http\Livewire\Tools;

use Livewire\Component;

class URLUnshortener extends Component
{
    public string $status = 'none';

    public string $result = '';

    public string $url = '';

    public function submit(): void
    {
        if ($this->url) {
            try {
                $ch = curl_init($this->url);
                curl_setopt_array($ch, [
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ]);
                curl_exec($ch);
                $effectiveUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                curl_close($ch);

                if ($effectiveUrl != $this->url) {
                    $this->result = $effectiveUrl;
                    $this->status = 'success';
                }
            } catch(\Exception $e) {
                $this->status = 'failure';
            }
        }
    }

    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('modules.tools.url-unshortener.livewire');
    }
}

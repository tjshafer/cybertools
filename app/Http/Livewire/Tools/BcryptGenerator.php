<?php

namespace App\Http\Livewire\Tools;

use Livewire\Component;

class BcryptGenerator extends Component
{
    public ?string $content = '';

    public ?string $hash = '';

    public function generate(): void
    {
        $this->hash = bcrypt($this->content);
    }

    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('modules.tools.bcrypt-generator.livewire');
    }
}

<?php

namespace App\Http\Livewire\Tools;

use Livewire\Component;

class HashGenerator extends Component
{
    public ?string $type = 'md2';

    public ?string $content = '';

    public ?string $hash = '';

    public function generate(): void
    {
        $this->hash = hash($this->type, $this->hash);
    }

    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('modules.tools.hash-generator.livewire');
    }
}

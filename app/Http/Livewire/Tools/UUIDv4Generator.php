<?php

namespace App\Http\Livewire\Tools;

use Illuminate\Support\Str;
use Livewire\Component;

class UUIDv4Generator extends Component
{
    public string $uuidv4 = '';

    public function submit(): void
    {
        $this->uuidv4 = Str::uuid()->toString();
    }

    public function mount(): void
    {
        $this->uuidv4 = Str::uuid()->toString();
    }

    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('modules.tools.uuidv4-generator.livewire');
    }
}

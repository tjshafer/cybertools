<?php

namespace App\Providers;

use App\Models\BlogPost;
use App\Models\Page;
use App\Models\User;
use App\Settings\GeneralSettings;
use App\Settings\LanguageSettings;
use App\Settings\SMTPSettings;
use Filament\Facades\Filament;
use Filament\Forms\Components;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use RyanChandler\FilamentTools\Tool;
use RyanChandler\FilamentTools\ToolInput;
use RyanChandler\FilamentTools\Tools;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Schema::defaultStringLength(191);

        if (is_cloudflare_https()) {
            URL::forceScheme('https');
        }

        $default = app(LanguageSettings::class)->defaultLanguage;
        $selected = Cookie::get('lang');

        if ($selected && is_valid_locale($selected)) {
            App::setLocale($selected);
        } else {
            App::setLocale($default);
        }

        $smtpSettings = app(SMTPSettings::class);
        $generalSettings = app(GeneralSettings::class);

        Config::set('app.name', $generalSettings->websiteTitle);

        if ($smtpSettings->enabled) {
            $config = [
                'driver' => 'smtp',
                'host' => $smtpSettings->host,
                'port' => $smtpSettings->port,
                'from' => [
                    'address' => $smtpSettings->from,
                    'name' => $smtpSettings->name,
                ],
                'encryption' => $smtpSettings->encryption,
                'username' => $smtpSettings->username,
                'password' => $smtpSettings->password,
            ];

            Config::set('mail', $config);
        }

        Paginator::defaultView('components.pagination-links');

        Route::model('page', Page::class);
        Route::model('post', BlogPost::class);

        Filament::serving(function (): void {
            Filament::registerTheme(mix('css/theme.css'));
        });

        Filament::registerNavigationGroups([
            'Administration',
            'Content',
            'Settings',
        ]);

        Tools::can(fn (User $user): bool => $user->admin || $user->super_admin);

        Tools::navigationIcon('heroicon-o-scissors');
        Tools::navigationGroup('Administration');

        Tools::register(fn (Tool $tool): Tool => $tool->label('Sitemap Generator')->schema([
            Components\ViewField::make('submission')->view('filament.tools.sitemapAlert')->label(''),
        ])->onSubmit(function (ToolInput $input) {
            generate_new_sitemap();

            $input->notify('success', 'Generated a new Sitemap successfully.');
        }));

        Tools::register(fn (Tool $tool): Tool => $tool->label('Symlink Generator')->schema([
            Components\ViewField::make('submission')->view('filament.tools.symlinkAlert')->label(''),
        ])->onSubmit(function (ToolInput $input) {
            if (function_exists('symlink')) {
                if (File::isDirectory(public_path('storage')) && File::isEmptyDirectory(public_path('storage'))) {
                    File::deleteDirectory(public_path('storage'));
                }

                Artisan::call('storage:link');

                $input->notify('success', 'Added a symlink successfully.');
            } else {
                $input->notify('danger', 'Symlink could not be created as the "symlink" function is disabled.');
            }
        }));

        Tools::register(fn (Tool $tool): Tool => $tool->label('Destroy')->schema([
            Components\Select::make('objects')->label('Objects to Destroy')->options([
                'cache' => 'Cache',
                'view-cache' => 'Views Cache',
                'config-cache' => 'Config Cache',
            ])->required(),
        ])->onSubmit(function (ToolInput $input) {
            if ($input->get('objects') == 'cache') {
                cache()->flush();

                $input->notify('success', 'Successfully destroyed cache.');
            } elseif ($input->get('objects') == 'view-cache') {
                Artisan::call('view:clear');

                $input->notify('success', 'Views Cache destroyed successfully.');
            } elseif ($input->get('objects') == 'config-cache') {
                Artisan::call('config:clear');

                $input->notify('success', 'Configuration Cache destroyed successfully.');
            }
        }));

        if (auth()->check()) {
            $user = User::find(auth()->user()->id);

            if ($user->allow_till < now()) {
                $user->allow_till = null;

                $user->save();
            }
        }
    }
}

<?php

namespace App\Filament\Extensions;

use Filament\Resources\Pages\Page;
use Illuminate\Database\Eloquent\Collection;

class Reorderable extends Page
{
    protected static string $titleField = 'title';

    protected static string $view = 'filament.order-page';

    protected static ?string $title = 'Re-order Entries';

    public Collection $records;

    public function mount(): void
    {
        $this->records = static::getResource()::getModel()::oldest('order')->get();
    }

    public function moveUp($i): void
    {
        if (isset($this->records[$i]) && $i > 0) {
            $temp = $this->records[$i]->order;

            $this->records[$i]->order = $this->records[$i - 1]->order;
            $this->records[$i - 1]->order = $temp;

            $this->records[$i]->save();
            $this->records[$i - 1]->save();

            $temp = $this->records[$i];
            $this->records[$i] = $this->records[$i - 1];
            $this->records[$i - 1] = $temp;
        }
    }

    public function moveDown($i): void
    {
        if (isset($this->records[$i]) && $i < (count($this->records) - 1)) {
            $temp = $this->records[$i]->order;

            $this->records[$i]->order = $this->records[$i + 1]->order;
            $this->records[$i + 1]->order = $temp;

            $this->records[$i]->save();
            $this->records[$i + 1]->save();

            $temp = $this->records[$i];
            $this->records[$i] = $this->records[$i + 1];
            $this->records[$i + 1] = $temp;
        }
    }
}

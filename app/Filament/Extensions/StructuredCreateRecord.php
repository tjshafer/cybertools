<?php

namespace App\Filament\Extensions;

class StructuredCreateRecord extends BitflanCreateRecord
{
    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data = parent::mutateFormDataBeforeCreate($data);

        $last = $this->getModel()::latest('order')->first();

        $data['order'] = $last ? $last->order + 1 : 1;

        return $data;
    }
}

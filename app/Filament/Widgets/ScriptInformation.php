<?php

namespace App\Filament\Widgets;

use Filament\Widgets\Widget;

class ScriptInformation extends Widget
{
    protected static ?int   $sort = 4;

    protected static string $view = 'filament.widgets.script-information';

    public string  $name;

    public string  $url;

    public ?string $version;

    public string  $vendor;

    public string  $vendor_url;

    public function mount(): void
    {
        $this->name = 'site';
        $this->url = 'https://bitflan.com';
        $this->version = number_format(1, 1);
        $this->vendor = '';
        $this->vendor_url = '';
    }
}

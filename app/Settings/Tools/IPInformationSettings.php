<?php

namespace App\Settings\Tools;

class IPInformationSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-ip-information';
    }
}

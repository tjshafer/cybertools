<?php

namespace App\Settings\Tools;

class CreditCardValidatorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-credit-card-validator';
    }
}

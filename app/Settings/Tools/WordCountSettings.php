<?php

namespace App\Settings\Tools;

class WordCountSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-word-count';
    }
}

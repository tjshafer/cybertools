<?php

namespace App\Settings\Tools;

class RedirectCheckerSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-redirect-checker';
    }
}

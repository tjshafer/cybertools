<?php

namespace App\Settings\Tools;

class JPGToWEBPSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-jpg-to-webp';
    }
}

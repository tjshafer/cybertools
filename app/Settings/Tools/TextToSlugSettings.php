<?php

namespace App\Settings\Tools;

class TextToSlugSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-text-to-slug';
    }
}

<?php

namespace App\Settings\Tools;

class PNGToJPGSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-png-to-jpg';
    }
}

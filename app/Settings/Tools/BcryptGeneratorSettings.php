<?php

namespace App\Settings\Tools;

class BcryptGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-bcrypt-generator';
    }
}

<?php

namespace App\Settings\Tools;

class SQLBeautifierSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-sql-beautifier';
    }
}

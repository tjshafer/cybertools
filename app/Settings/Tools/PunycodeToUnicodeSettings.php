<?php

namespace App\Settings\Tools;

class PunycodeToUnicodeSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-punycode-to-unicode';
    }
}

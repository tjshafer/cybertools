<?php

namespace App\Settings\Tools;

class URLExtractorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-url-extractor';
    }
}

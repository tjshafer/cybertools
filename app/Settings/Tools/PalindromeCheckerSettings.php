<?php

namespace App\Settings\Tools;

class PalindromeCheckerSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-palindrome-checker';
    }
}

<?php

namespace App\Settings\Tools;

class EmailValidatorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-email-validator';
    }
}

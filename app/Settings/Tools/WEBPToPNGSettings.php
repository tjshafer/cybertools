<?php

namespace App\Settings\Tools;

class WEBPToPNGSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-webp-to-png';
    }
}

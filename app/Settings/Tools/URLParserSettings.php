<?php

namespace App\Settings\Tools;

class URLParserSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-url-parser';
    }
}

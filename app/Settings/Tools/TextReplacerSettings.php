<?php

namespace App\Settings\Tools;

class TextReplacerSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-text-replacer';
    }
}

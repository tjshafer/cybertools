<?php

namespace App\Settings\Tools;

class CaseConverterSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-case-converter';
    }
}

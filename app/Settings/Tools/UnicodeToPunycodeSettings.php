<?php

namespace App\Settings\Tools;

class UnicodeToPunycodeSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-unicode-to-punycode';
    }
}

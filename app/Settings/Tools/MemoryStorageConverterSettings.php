<?php

namespace App\Settings\Tools;

class MemoryStorageConverterSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-memory-storage-converter';
    }
}

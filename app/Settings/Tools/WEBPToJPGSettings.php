<?php

namespace App\Settings\Tools;

class WEBPToJPGSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-webp-to-jpg';
    }
}

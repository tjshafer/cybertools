<?php

namespace App\Settings\Tools;

class ImageResizerSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-image-resizer';
    }
}

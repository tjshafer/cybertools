<?php

namespace App\Settings\Tools;

class PasswordStrengthTestSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-password-strength-test';
    }
}

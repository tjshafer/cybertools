<?php

namespace App\Settings\Tools;

class TwitterCardGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-twitter-card-generator';
    }
}

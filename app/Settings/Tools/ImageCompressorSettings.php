<?php

namespace App\Settings\Tools;

class ImageCompressorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-image-compressor';
    }
}

<?php

namespace App\Settings\Tools;

class SourceCodeDownloaderSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-source-code-downloader';
    }
}

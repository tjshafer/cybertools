<?php

namespace App\Settings\Tools;

class YouTubeThumbnailDownloaderSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-youtube-thumbnail-downloader';
    }
}

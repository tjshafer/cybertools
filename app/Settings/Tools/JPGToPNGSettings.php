<?php

namespace App\Settings\Tools;

class JPGToPNGSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-jpg-to-png';
    }
}

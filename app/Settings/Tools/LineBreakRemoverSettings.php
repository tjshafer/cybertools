<?php

namespace App\Settings\Tools;

class LineBreakRemoverSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-line-break-remover';
    }
}

<?php

namespace App\Settings\Tools;

class PrivacyPolicyGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-privacy-policy-generator';
    }
}

<?php

namespace App\Settings\Tools;

class WordDensityCounterSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-word-density-counter';
    }
}

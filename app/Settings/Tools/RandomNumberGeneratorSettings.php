<?php

namespace App\Settings\Tools;

class RandomNumberGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-random-number-generator';
    }
}

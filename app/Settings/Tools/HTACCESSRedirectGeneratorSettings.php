<?php

namespace App\Settings\Tools;

class HTACCESSRedirectGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-htaccess-redirect-generator';
    }
}

<?php

namespace App\Settings\Tools;

class PNGToWEBPSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-png-to-webp';
    }
}

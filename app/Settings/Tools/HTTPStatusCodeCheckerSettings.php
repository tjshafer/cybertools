<?php

namespace App\Settings\Tools;

class HTTPStatusCodeCheckerSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-http-status-code-checker';
    }
}

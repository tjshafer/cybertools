<?php

namespace App\Settings\Tools;

class TermsOfServiceGeneratorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-terms-of-service-generator';
    }
}

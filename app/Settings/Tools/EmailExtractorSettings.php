<?php

namespace App\Settings\Tools;

class EmailExtractorSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-email-extractor';
    }
}

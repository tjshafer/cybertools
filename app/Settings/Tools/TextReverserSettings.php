<?php

namespace App\Settings\Tools;

class TextReverserSettings extends BaseToolSetting
{
    public static function group(): string
    {
        return 'tool-text-reverser';
    }
}
